# TranferMate
## Test task solution + from scratch mirco MVC framework

TransferMate app with micro MVC framework integrated has be made for testing purposes only.
It's lightweith microfamework is basically made of the following classes working together:
- Core.php wich in the same time works as router and dispatcher.
- BaseController 
- HTTPCotnroller handles requests comming from HTTP
- ConsoleController handles requests comming from console
- Database handling the database connection via PDO
- BaseModel

The project structure is very simple and understandable, all the classes have been writen with SOLID principles in mind.
The framework is made to be highly extendible.

##### The project uses composer only for PSR-4 autoload only, and it's highly and wide used during my experience.



## Installation:

Import the dump.sql into .
Update the `app/config/database.php` file with your database settings.

Run the following commands:

```
cd transfermate
composer install
cd /public
php -S localhost:8080
```

###### Go to : http://localhost:8080 on your browser, and you must se the Hello message

## Features
- Import books by console 
- Search books in broswer
- Supports korean and cyrilic
 


## Serch page: 

http://localhost:8080/xml


## Run using console

Navigate te public folder via terminal
 
```
cd public
```

### Store books by using one specific file
```
php index.php XML storeBook <xml_file_path>
```

### Store book recusrsively. Specify xml working dir

```
php index.php XML storeBooks <xml_dir>
```