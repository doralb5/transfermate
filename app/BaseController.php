<?php

namespace TransferMate;

use TransferMate\Interfaces\ModelInterface;

class BaseController
{

    public function model($model)
    {
        $modelClass = "TransferMate\\Models\\" . $model;
        if (!class_exists($modelClass)) {
            throw new \Exception("Model $model not found!");
        }
        return new $modelClass;
    }

}
