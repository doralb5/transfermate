<?php

namespace TransferMate;

use TransferMate\Interfaces\ModelInterface;

class BaseModel implements ModelInterface
{

    public $table;

    private $db;

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    public function getAll()
    {
        $this->db->query("SELECT * FROM books.$this->table");
        $result = $this->db->result();
        return $result;
    }

    public function getDb()
    {
        return $this->db;
    }

    public function save($data)
    {
        $fields = '';
        $values = '';

        foreach ($data as $key => $value) {
            $fields .= " $key,";
            $values .= " ?,";
        }

        $fields = rtrim($fields, ',');
        $values = rtrim($values, ',');

        $sql = "INSERT INTO " . $this->table . "( $fields ) VALUES ( $values )";

        $this->db->query($sql);

        $i = 1;
        foreach ($data as $value) {
            $this->db->bind($i, $value);
            $i++;
        }

        $this->db->execute();
        return $this->db->lastInsertedId();
    }

    public function find($parameter = [])
    {
        $field = key($parameter);
        $value = $parameter[$field];
        $sql = "SELECT * FROM " . $this->table . " WHERE $field = ?";
        return $this->db->query($sql)
            ->bind(1, $value)
            ->single();
    }

    public function search(array $params = [], $limit = 20, $offset = 0)
    {
        $sql = "SELECT * FROM " . $this->table . " WHERE true ";

        foreach ($params as $key => $value) {
            $sql .= " AND LOWER(" . $key . ") like LOWER(?)";
        }

        $sql .= " LIMIT $limit OFFSET $offset";

        $this->db->query($sql);

        $i = 1;
        foreach ($params as $value) {
            $this->db->bind($i, '%' . $value . '%');
            $i++;
        }

        return $this->db->result();
    }
}
