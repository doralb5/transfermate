<?php

namespace TransferMate;

use TransferMate\Exception\CoreException;

/**
 * Core APP class
 */
class Core
{
    protected $currentController = "Default";
    protected $currentMethod = "index";
    protected $params = [];
    protected $client = "browser";
    protected $controllerType = '';


    public function __construct()
    {

        if (PHP_SAPI === 'cli') {
            $argv = $_SERVER['argv'];
            $this->currentController = isset($argv[1]) ? ucwords($argv[1]) : $this->currentController;
            $this->currentMethod = $argv[2] ?? $this->currentMethod;
            unset($argv[0], $argv[1], $argv[2]);
            $this->params = $argv ? array_values($argv) : [];
            $this->controllerType = 'Console';
        } else {
            $urlInfo = $this->getUrl();
            $this->currentController = $urlInfo[0] ? ucwords($urlInfo[0]) : $this->currentController;
            $this->currentMethod = $urlInfo[1] ?? $this->currentMethod;
            unset($urlInfo[0], $urlInfo[1]);
            $this->params = $urlInfo ? array_values($urlInfo) : [];
            $this->controllerType = 'HTTP';
        }

        $controllerName = $this->currentController . 'Controller';
        $controllerFile = APP_PATH . "controller" . DS . strtolower($this->controllerType) . "/" . $controllerName . ".php";


        if (!file_exists($controllerFile)) {
            throw new CoreException("Controller $controllerName not found");
        }

        $controllerClass  = "TransferMate\\Controller\\$this->controllerType\\" . $controllerName;
        if (!class_exists($controllerClass)) {
            throw new CoreException("Controller Class does not exist.");
        }

        $this->currentController = new $controllerClass;
        if (!method_exists($this->currentController, $this->currentMethod)) {
            throw new CoreException("Method does not exist.");
        }
    }

    public function dispatch()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            header('Content-Type: application/json');
        }
        
        call_user_func_array(
            [
                $this->currentController,
                $this->currentMethod
            ],
            $this->params
        );
    }

    public function getUrl()
    {

        // Any server
        if (isset($_SERVER['REQUEST_URI'])) {
            $url = trim($_SERVER['REQUEST_URI'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);

            // Here we can create a url object
            $urlInfo = explode('/', $url);

            return $urlInfo;
        }

        // Apache servers with .httacess

        if (isset($_GET['url'])) {
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $urlInfo = explode('/', $url);
            return $urlInfo;
        }
    }
}
