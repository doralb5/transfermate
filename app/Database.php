<?php

namespace TransferMate;

use \PDO;

class Database
{

    private $dbHost = DB_HOST;
    private $dbUser = DB_USER;
    private $dbPass = DB_PASSWORD;
    private $dbName = DB_NAME;

    private static $_instance;

    private $statement;
    private $dbHandler;
    private $error;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone()
    {
    }

    function __destruct()
    {
        // Close database connection
        $this->dbHandler = null;
    }

    public function __construct()
    {
        $conn = DB_DRIVER . ":host=" . $this->dbHost . ";dbname=" . $this->dbName;
        $options = [
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ];

        try {
            $this->dbHandler = new PDO($conn, $this->dbUser, $this->dbPass, $options);
        } catch (\PDOException $e) {
            $this->error = $e->getMessage();
            die($this->error);
        }
    }

    public function query($sql)
    {
        $this->statement = $this->dbHandler->prepare($sql);
        return $this;
    }

    public function bind($parameter, $value, $type = null)
    {
        switch (is_null($type)) {
            case is_int($value):
                $type = PDO::PARAM_INT;
                break;
            case is_bool($value):
                $type = PDO::PARAM_BOOL;
                break;
            case is_null($value):
                $type = PDO::PARAM_NULL;
                break;
            default:
                $type = PDO::PARAM_STR;
        }

        $this->statement->bindValue($parameter, $value, $type);
        return $this;
    }

    public function execute()
    {
        return $this->statement->execute();
    }

    public function result()
    {
        $this->execute();
        return $this->statement->fetchAll(PDO::FETCH_OBJ);
    }

    public function single()
    {
        $this->execute();
        return $this->statement->fetch(PDO::FETCH_OBJ);
    }

    public function count()
    {
        return $this->statement->rowCount();
    }

    public function lastInsertedId()
    {
        return $this->dbHandler->lastInsertId();
    }
}
