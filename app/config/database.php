<?php

define('DB_DRIVER', 'pgsql');
define('DB_HOST', 'localhost');
define('DB_PORT', 5432);
define('DB_NAME', 'transfermate');
define('DB_USER', 'postgres');
define('DB_PASSWORD', 'password');