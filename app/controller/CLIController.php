<?php

namespace TransferMate\Controller;

use TransferMate\BaseController;

class CLIController extends BaseController
{
    public function say($str, $type = '')
    {
        switch ($type) {
            case 'success':
                echo "\e[1;32m$str\e[0m\n";
                break;
            case 'warning':
                echo "\e[1;33m$str\e[0m\n";
                break;
            case 'error':
                echo "\e[0;31m$str\e[0m\n";
                break;
            default:
                echo $str;
                break;
        }

        echo PHP_EOL;
    }
}
