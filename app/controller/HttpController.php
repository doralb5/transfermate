<?php

namespace TransferMate\Controller;

use TransferMate\BaseController;
use TransferMate\Interfaces\HTTPRequestInterface;

class HTTPController extends BaseController implements HTTPRequestInterface
{

    public function view($view, $data = [])
    {
        $viewPath = APP_PATH . "views" . DS . $view . '.php';
        if (!file_exists($viewPath)) {
            throw new \Exception("View $view not found!");
        }

        require_once $viewPath;
    }

    public function jsonResponse($data)
    {
        echo json_encode(json_decode(json_encode($data), TRUE));
    }
}
