<?php

namespace TransferMate\Controller\Console;

use TransferMate\Controller\CLIController;

class DefaultController extends CLIController{
 
    public function index(){
        $this->say("Hi from console", 'success');
    }
    
}