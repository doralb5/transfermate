<?php

namespace TransferMate\Controller\Console;

use TransferMate\Controller\CLIController;

class XMLController extends CLIController
{

    private $content;
    private $bookModel;
    private $authorModel;


    public function __construct()
    {
        $this->bookModel = $this->model('Book');
        $this->authorModel = $this->model('Author');
    }

    public function storeBooks($dirPath)
    {
        $fullDirPath = PUBLIC_PATH . str_replace('/', DS, $dirPath);
        $it = new \RecursiveDirectoryIterator($fullDirPath);
        $xmlFiles = [];
        foreach (new \RecursiveIteratorIterator($it) as $file) {
            if ($file->getExtension() == 'xml') {
                $filePath = str_replace(PUBLIC_PATH, '', $file->getRealPath());
                $xmlFiles[] = $filePath;
            }
        }

        foreach ($xmlFiles as $xmlFile) {
            $this->storeBook($xmlFile);
        }
    }

    public function storeBook($filePath = null)
    {

        try {
            $result = $this->readFile($filePath);
        } catch (\Exception $e) {
            $this->say("Error while reading XML file: $filePath " . $e->getMessage(), 'error');
            return;
        }

        $books = reset($result);

        foreach ($books as $book) {
            $author = [];
            $author['name'] = $book['author'];

            // Check if the author exists in db
            $authorDb = $this->authorModel->find(['name' => $book['author']]);

            if (!$authorDb) {
                // Insert new author
                $author['id'] = $this->authorModel->save($author);

                if (!$author['id']) {
                    throw new \Exception("Failed to insert author" . $author['name']);
                }

                $this->say("New author: " . $author["name"] . " has been inserted");
            } else {
                $author['id'] = $authorDb->id;
            }

            $bookDb = $this->bookModel->getBookByAuthorIdTitle($author['id'], $book['name']);

            if ($bookDb) {
                $this->say("Book " . $book['name'] . " by " . $author["name"] . " has already been inserted into database", 'warning');
                continue;
            }

            $newBook = [];
            $newBook['title'] = $book['name'];
            $newBook['author_id'] = $author['id'];
            $newBookId = $this->bookModel->save($newBook);
            if ($newBookId) {
                $this->say("New book: " .  $newBook['title'] . " by " . $author["name"] . " has been inserted into database", 'success');
            }
        }
    }


    private function readFile($filePath)
    {
        if (!$filePath) {
            throw new \Exception("A filepath must be provided");
        }
        $fullPath =  PUBLIC_PATH . str_replace('/', DS, $filePath);
        if (!file_exists($fullPath)) {
            throw new \Exception("File does not exist");
        }
        $this->content = file_get_contents($fullPath);
        $xml = simplexml_load_string($this->content, "SimpleXMLElement", LIBXML_NOCDATA);
        return json_decode(json_encode($xml), TRUE);
    }
}
