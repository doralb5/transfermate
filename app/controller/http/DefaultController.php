<?php

namespace Transfermate\Controller\HTTP;

use TransferMate\Controller\HTTPController;

class DefaultController extends HTTPController{
 
    public function index(){
        $this->view('simpleview');
    }
    
}