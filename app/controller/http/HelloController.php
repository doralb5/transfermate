<?php

namespace TransferMate\Controller\HTTP;

use TransferMate\Controller\HTTPController;

class HelloController extends HTTPController{

    public function index($arg1 = null){
        $model  = $this->model('Book');
        $result = $model->getAll();
        print_r($result);
    }

}