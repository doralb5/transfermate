<?php

namespace TransferMate\Controller\HTTP;

use TransferMate\Controller\HTTPController;

class XMLController extends HTTPController
{

    public function index()
    {
        $this->view('xml/index');
    }


    public function searchAjax($q = '')
    {
        $q = urldecode($q);
        $bookModel = $this->model('Book');
        $books = $bookModel->searchBooks($q);
        $this->jsonResponse($books);
    }
}