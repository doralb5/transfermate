<?php

namespace TransferMate\Models;

use TransferMate\BaseModel;

class Author extends BaseModel{

    public $table = 'authors';
    public $fields = [
        'id',
        'name'
    ];
}