<?php

namespace TransferMate\Models;

use TransferMate\BaseModel;

class Book extends BaseModel
{

    public $table = 'books';

    public $fileds = [
        'id',
        'title',
        'author_id'
    ];


    public function searchBooks($q)
    {
        $q = (string) $q;
        $sql = "SELECT authors.name as author_name, books.title FROM " . $this->table . " RIGHT JOIN authors on books.author_id = authors.id WHERE LOWER(books.title) like LOWER(?) OR lower(authors.name) like LOWER(?) ";
        $result = $this->getDb()->query($sql)
            ->bind(1, "%$q%")
            ->bind(2, "%$q%")
            ->result();
        return $result;
    }

    public function getBookByAuthorIdTitle($author_id, $book_title)
    {

        $sql = "SELECT * FROM books WHERE author_id = ? AND title = ?";

        return $this->getDb()
            ->query($sql)
            ->bind(1, $author_id)
            ->bind(2, $book_title)
            ->single();
    }
}
