<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>A simple view</title>
</head>
<body>
        Hello from view!
        <br>
        <br>
        What do you want to do now ?

        <ul>
            <li><a href="/xml">Search books?</a></li>
        </ul>
</body>
</html>