<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="/">
    <title>Search Books</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="container-fluid bg-gray mt-5 tall-20">
        <h2 class="text-center">Search for books</h2>
        <div class="text-center">
            <input type="text" id="search" name="q" placeholder="Enter title and hit Enter">
        </div>

        <div class="text-center mt-5">
            <table class="table">
                <thead>
                    <tr>
                        <th width="50%">Author</th>
                        <th>Book Title</th>
                    </tr>
                </thead>
                <tbody id="results">

                </tbody>
            </table>
        </div>

    </div>

    <footer class="container-fluid stick-bottom">
        Copyright &copy; Doralb Kurti - TransferMate Test
    </footer>
    <script src="javascript/script.js"></script>
</body>

</html>