<?php

require_once '../app/config/definitons.php';
require_once '../app/config/database.php';
require_once '../vendor/autoload.php';


$core = new TransferMate\Core;
$core->dispatch();