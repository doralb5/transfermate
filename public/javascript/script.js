// Add event listener
document.getElementById('search').onkeypress = function(e) {
    var ajaxUrl = "/xml/searchAjax/";
    if (!e) e = window.event;
    var keyCode = e.code || e.key;
    var q = document.getElementById('search').value;
    ajaxUrl = ajaxUrl + q;
    if (keyCode == 'Enter' || keyCode == 'NumpadEnter') {
        ajax(ajaxUrl, "displayResult");
        return false;
    }
}

// Display resulsts function
function displayResult(response) {
    if (!Object.keys(response).length) {
        alert("no books found");
        return;
    }

    var tableBody = document.getElementById('results');
    tableBody.innerHTML = '';

    var book;
    for (var i in response) {
        book = response[i];
        addBookRow(book);
    }
}


function addBookRow(book) {
    var row = "<tr><td>" + book.author_name + '</td><td>' + (book.title == null ? '&#x3C;none&#x3E; (no books found)' : book.title) + '</td></tr>';
    var tableBody = document.getElementById('results');
    tableBody.innerHTML = tableBody.innerHTML + row;
}

// Ajax request function
function ajax(url, callback, method = "GET") {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            if (xmlhttp.status == 200) {
                window[callback](JSON.parse(xmlhttp.responseText));
            } else {
                alert('something went wrong. Status:' + xmlhttp.status);
            }
        }
    };
    xmlhttp.open(method, url, true);
    xmlhttp.setRequestHeader('x-requested-with', 'xmlhttprequest');
    xmlhttp.send();
}